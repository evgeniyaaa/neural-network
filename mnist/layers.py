
import numpy as np
from theano.tensor.nnet.nnet import sigmoid
from theano.tensor.nnet.nnet import softmax
from theano.tensor.nnet import conv2d
from theano.tensor.signal.pool import pool_2d
import theano
import theano.tensor as T


def ReLU(z):
    return T.maximum(0.0, z)


def linear(z):
    return z


class SoftmaxLayer:
    def __init__(self, n_in, n_out, activation_fn=softmax):
        self.n_in = n_in
        self.n_out = n_out
        self.activation_fn = activation_fn
        self.initialize_params()
        self.params = [self.w, self.b]

    def initialize_params(self):
        self.w = theano.shared(
            np.asarray(
                np.zeros((self.n_in, self.n_out)),
                dtype=theano.config.floatX
            ), name='w', borrow=True)
        self.b = theano.shared(
            np.asarray(
                np.zeros((self.n_out,)),
                dtype=theano.config.floatX
            ), name='b', borrow=True)

    def set_input(self, inpt, mini_batch_size):
        inpt = inpt.reshape((mini_batch_size, self.n_in))
        self.activation = self.activation_fn(
            T.dot(inpt, self.w) + self.b
        )
        self.y_out = T.argmax(self.activation, axis=1)

    def accuracy(self, y):
        return T.mean(T.eq(y, self.y_out))

    def cost(self, net):
        a = T.log(self.activation)[T.arange(net.y.shape[0]), net.y]
        return -T.mean(a)


class FullyConnectedLayer:
    def __init__(self, n_in, n_out, activation_fn=sigmoid):
        self.n_in = n_in
        self.n_out = n_out
        self.activation_fn = activation_fn
        self.initialize_params()
        self.params = [self.w, self.b]

    def initialize_params(self):
        self.w = theano.shared(
            np.asarray(
                np.random.normal(0, np.sqrt(1 / self.n_out), (self.n_in, self.n_out)),
                dtype=theano.config.floatX
            ), name='w', borrow=True)
        self.b = theano.shared(
            np.asarray(
                np.random.normal(0, 1, (self.n_out,)),
                dtype=theano.config.floatX
            ), name='b', borrow=True)

    def set_input(self, inpt, mini_batch_size):
        inpt = inpt.reshape((mini_batch_size, self.n_in))
        self.activation = self.activation_fn(
            T.dot(inpt, self.w) + self.b
        )


class ConvPoolLayer:
    def __init__(self, input_shape, filter_shape, poolsize=(2, 2), activation_fn=linear):
        # mini_batch_size, num_input_feature_maps, image_height, image_width
        self.input_shape = input_shape
        # num_filters, num_input_feature_maps, filter_height, filter_width
        self.filter_shape = filter_shape
        self.poolsize = poolsize
        self.activation_fn = activation_fn
        self.initialize_params()
        self.params = [self.w, self.b]

    def initialize_params(self):
        n_out = (self.filter_shape[0] * np.prod(self.filter_shape[2:]) / np.prod(self.poolsize))
        self.w = theano.shared(
            np.asarray(
                np.random.normal(0, np.sqrt(1.0 / n_out), size=self.filter_shape),
                dtype=theano.config.floatX), borrow=True)
        self.b = theano.shared(
            np.asarray(
                np.random.normal(0, 1, size=(self.filter_shape[0],)),
                dtype=theano.config.floatX), borrow=True)

    def set_input(self, inpt, mini_batch_size):
        # print(inpt)
        inpt = inpt.reshape(self.input_shape)
        conv_out = conv2d(
            input=inpt, filters=self.w, filter_shape=self.filter_shape, input_shape=self.input_shape
        )
        # print(self.input_shape)
        pooled_out = pool_2d(
            input=conv_out, ws=self.poolsize, ignore_border=True
        )
        self.activation = self.activation_fn(
            pooled_out + self.b.dimshuffle('x', 0, 'x', 'x')
        )
