
import pickle
import gzip
import numpy as np
import theano
import theano.tensor as T

from layers import FullyConnectedLayer, SoftmaxLayer, ConvPoolLayer


def size(data):
    return data[0].get_value().shape[0]


class Network:
    def __init__(self, layers, mini_batch_size):
        self.layers = layers

        self.x = T.matrix('x')
        self.y = T.ivector('y')

        init_layer = self.layers[0]
        init_layer.set_input(self.x, mini_batch_size)

        for i in range(1, len(self.layers)):
            self.layers[i].set_input(self.layers[i - 1].activation, mini_batch_size)

        self.activation = self.layers[-1].activation

    def SGD(self, training_data, epochs, mini_batch_size, learning_eta, reg_lambda, validation_data, test_data):

        training_x, training_y = training_data
        validation_x, validation_y = validation_data
        test_x, test_y = test_data

        num_training_batches = int(size(training_data) / mini_batch_size)
        num_validation_batches = int(size(validation_data) / mini_batch_size)
        num_test_batches = int(size(test_data) / mini_batch_size)

        params = [param for layer in self.layers for param in layer.params]
        l2_norm_squared = sum([(layer.w ** 2).sum() for layer in self.layers])
        cost = self.layers[-1].cost(self) + 0.5 * reg_lambda * l2_norm_squared / num_training_batches
        grads = T.grad(cost, params)
        updates = [
            [param, param - learning_eta * grad]
            for param, grad in zip(params, grads)
        ]

        i = T.iscalar()
        train_md = theano.function(
            [i], cost, updates=updates,
            givens={
                self.x: training_x[i * mini_batch_size: (i + 1) * mini_batch_size],
                self.y: training_y[i * mini_batch_size: (i + 1) * mini_batch_size]
            }
        )

        # Validations
        validate_mb_accuracy = theano.function(
            [i], self.layers[-1].accuracy(self.y),
            givens={
                self.x: validation_x[i * mini_batch_size: (i + 1) * mini_batch_size],
                self.y: validation_y[i * mini_batch_size: (i + 1) * mini_batch_size]
            }
        )
        test_mb_accuracy = theano.function(
            [i], self.layers[-1].accuracy(self.y),
            givens={
                self.x: test_x[i * mini_batch_size: (i + 1) * mini_batch_size],
                self.y: test_y[i * mini_batch_size: (i + 1) * mini_batch_size]
            }
        )

        best_validation_accuracy = 0.0

        for epoch in range(epochs):
            for mini_batch_index in range(num_training_batches):
                train_md(mini_batch_index)

                iteration = num_training_batches * epoch + mini_batch_index
                if iteration % 1000 == 0:
                    print('Training mini-batch number {0}'.format(iteration))

                # Validations
                # Ако се дели точно на num_training_batches
                if(iteration + 1) % num_training_batches == 0:
                    validation_accuracy = np.mean(
                        [validate_mb_accuracy(j)
                         for j in range(num_validation_batches)
                         ]
                    )
                    print('Epoch {0}: validation_accuracy {1:.2%}'.format(epoch, validation_accuracy))

                    if validation_accuracy >= best_validation_accuracy:
                        print('This is the best validation accuracy to date.')
                        best_validation_accuracy = validation_accuracy
                        best_iteration = iteration

                        test_accuracy = np.mean(
                            [test_mb_accuracy(j)
                             for j in range(num_test_batches)
                             ]
                        )
                        print('Epoch {0}: test_accuracy {1:.2%}'.format(epoch, test_accuracy))

        print('Best validation accuracy of {0:.2%} obtained at iteration {1}'.format(best_validation_accuracy, best_iteration))
        print('Corresponding test accuracy of {0:.2%}'.format(test_accuracy))


def load_data(filename='./data/mnist.pkl.gz'):
    f = gzip.open(filename, 'rb')
    training_data, validation_data, test_data = pickle.load(f, encoding='latin')
    f.close()

    def get_shared(data):
        x = theano.shared(
            np.asarray(data[0], dtype=theano.config.floatX), borrow=True
        )
        y = theano.shared(
            np.asarray(data[1], dtype=theano.config.floatX), borrow=True
        )
        return x, T.cast(y, 'int32')

    return [get_shared(training_data), get_shared(validation_data), get_shared(test_data)]


training_data, validation_data, test_data = load_data()

mini_batch_size = 10

net = Network(
    [
        ConvPoolLayer(
            input_shape=(mini_batch_size, 1, 28, 28),
            filter_shape=(20, 1, 5, 5),
            poolsize=(2, 2)
        ),
        FullyConnectedLayer(n_in=20 * 12 * 12, n_out=100),
        SoftmaxLayer(n_in=100, n_out=10)
    ],
    mini_batch_size
)

net.SGD(
    training_data,
    epochs=30,
    mini_batch_size=mini_batch_size,
    learning_eta=0.1,
    reg_lambda=0.1,
    validation_data=validation_data,
    test_data=test_data
)
