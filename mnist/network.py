import random
import numpy as np
import matplotlib.pyplot as plt
import mnist_loader


def create_graph(X, Y, title):
    plt.figure(figsize=(7, 4), facecolor='#F3ECF5')
    plt.plot(X, Y)
    plt.title(title)
    plt.show()


def sigmoid(z, prime=False):
    a = 1. / (1. + np.exp(-z))
    if prime:
        return a * (1 - a)
    return a


class CrossEntropyCost:
    @staticmethod
    def fn(a, y):
        return -np.sum(np.nan_to_num(y * np.log(a) + (1 - y) * np.log(1 - a)))

    @staticmethod
    def error(a, y):
        return a - y


class Network:
    def __init__(self, sizes, act_fns, cost):
        self.sizes = sizes
        self.act_fns = act_fns
        self.cost = cost
        self.initialize_parameters()

    def initialize_parameters(self):
        self.weights = [np.random.normal(0, 1 / np.sqrt(n_in), (n_out, n_in)) for n_in, n_out in zip(self.sizes[:-1], self.sizes[1:])]
        self.biases = [np.random.normal(0, 1, (n_out, 1)) for n_out in self.sizes[1:]]

    def SGD(self, training_data, epochs, mini_batch_size, learning_eta, reg_lambda, validation_data):
        self.learning_eta = learning_eta
        self.reg_lambda = reg_lambda
        self.N = len(training_data)

        if validation_data:
            n_data = len(validation_data)

        eval_cost, eval_accuracy = [], []
        training_cost, training_accuracy = [], []

        for j in range(epochs):
            random.shuffle(training_data)
            mini_batches = [
                training_data[k:mini_batch_size + k]
                for k in range(0, self.N, mini_batch_size)
            ]

            for mini_batch in mini_batches:
                self.update_mini_batch(mini_batch)
            print('\n Epoch %s training complete' % j)

            cost1 = self.total_cost(training_data)
            training_cost.append(cost1)
            print('Cost on training data: {}'.format(cost1))
            accuracy1 = self.accuracy(training_data, convert=True)
            training_accuracy.append(accuracy1)
            print('Accuracy on training data: {} / {}'.format(accuracy1, self.N))
            cost2 = self.total_cost(validation_data, convert=True)
            eval_cost.append(cost2)
            print('Cost on evaluation data: {}'.format(cost2))
            accuracy2 = self.accuracy(validation_data)
            eval_accuracy.append(accuracy2)
            print('Accuracy on evaluation data: {} / {}'.format(accuracy2, n_data))

        return eval_cost, eval_accuracy, training_cost, training_accuracy

    def update_mini_batch(self, mini_batch):

        der_w = [np.zeros(w.shape) for w in self.weights]
        der_b = [np.zeros(b.shape) for b in self.biases]

        for x, y in mini_batch:
            delta_der_w, delta_der_b = self.backpropagate(x, y)
            der_w = [dw + ddw for dw, ddw in zip(der_w, delta_der_w)]
            der_b = [db + ddb for db, ddb in zip(der_b, delta_der_b)]

        weight_decay = (1 - (self.learning_eta * self.reg_lambda) / self.N)
        self.weights = [
            weight_decay * w - (self.learning_eta / len(mini_batch)) * dw
            for w, dw in zip(self.weights, der_w)
        ]
        self.biases = [
            b - (self.learning_eta / len(mini_batch)) * db
            for b, db in zip(self.biases, der_b)
        ]

    def feedforward(self, x):
        zs = []
        act = x
        activations = [act]
        for i in range(len(self.act_fns)):
            w = self.weights[i]
            b = self.biases[i]
            z = np.dot(w, act) + b
            act = self.act_fns[i](z)
            zs.append(z)
            activations.append(act)

        return zs, activations

    def backpropagate(self, x, y):
        zs, activations = self.feedforward(x)

        der_w = [np.zeros(w.shape) for w in self.weights]
        der_b = [np.zeros(b.shape) for b in self.biases]

        error = self.cost.error(activations[-1], y)
        der_b[-1] = error
        der_w[-1] = np.dot(error, activations[-2].T)

        for l in range(2, len(self.sizes)):
            z = zs[-l]
            a = self.act_fns[-l](z, prime=True)
            error = np.dot(self.weights[-l + 1].T, error) * a
            der_b[-l] = error
            der_w[-l] = np.dot(error, activations[-l - 1].T)

        return der_w, der_b

    def evaluate_activations(self, a):
        for w, b in zip(self.weights, self.biases):
            a = sigmoid(np.dot(w, a) + b)
        return a

    def accuracy(self, data, convert=False):
        if convert:
            results = [
                (np.argmax(self.evaluate_activations(x)), np.argmax(y))
                for (x, y) in data
            ]
        else:
            results = [
                (np.argmax(self.evaluate_activations(x)), y)
                for (x, y) in data
            ]
        return sum(int(x == y) for (x, y) in results)

    def total_cost(self, data, convert=False):
        cost = 0.0
        for x, y in data:
            a = self.evaluate_activations(x)
            if convert:
                y = vectorized_result(y)
            cost += self.cost.fn(a, y) / len(data)
        cost += 0.5 * (self.reg_lambda / len(data)) * sum(np.linalg.norm(w) ** 2 for w in self.weights)
        return cost


def vectorized_result(j):
    e = np.zeros((10, 1))
    e[j] = 1.0
    return e


training_data, validation_data, test_data = mnist_loader.load_data_wrapper()
net = Network(
    sizes=[784, 30, 10],
    act_fns=[sigmoid, sigmoid],
    cost=CrossEntropyCost
)
epochs = 10
eval_cost, eval_accuracy, training_cost, training_accuracy = net.SGD(
    training_data,
    epochs=epochs,
    mini_batch_size=30,
    learning_eta=0.5,
    reg_lambda=5,
    validation_data=validation_data
)


create_graph(range(epochs), eval_cost, 'eval_cost')
create_graph(range(epochs), eval_accuracy, 'eval_accuracy')
create_graph(range(epochs), training_cost, 'training_cost')
create_graph(range(epochs), training_accuracy, 'training_accuracy')
