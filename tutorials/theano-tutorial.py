from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import theano

import theano.tensor as T

print(theano.config.device)

foo = T.scalar('foo')
bar = foo ** 2

# print(type(bar))
# print(bar.type)
# print(theano.pp(bar))

f = theano.function([foo], bar)
# print(f(4))
# print(bar.eval({foo: 3}))


def square(x):
    return x ** 2


bar = square(foo)
# print(bar.eval({foo: 3}))

A = T.matrix('A')
x = T.vector('x')
b = T.vector('b')
y = T.dot(A, x) + b
z = T.sum(A ** 2)

b_default = np.array([0, 0], dtype=theano.config.floatX)
linear_mix = theano.function([A, x, theano.In(b, value=b_default)], [y, z])

linear_mix_full = linear_mix(
    np.array([[1, 2, 3],
              [4, 5, 6]], dtype=theano.config.floatX),  # A
    np.array([1, 2, 3], dtype=theano.config.floatX),  # x
    np.array([4, 5], dtype=theano.config.floatX)  # b
)
linear_mix_default = linear_mix(
    np.array([[1, 2, 3],
              [4, 5, 6]], dtype=theano.config.floatX),  # A
    np.array([1, 2, 3], dtype=theano.config.floatX)  # x
)
# print(linear_mix_full)
# print(linear_mix_default)

shared_var = theano.shared(np.array([[1, 2], [3, 4]], dtype=theano.config.floatX))
shared_var.set_value(np.array([[3, 4], [2, 1]], dtype=theano.config.floatX))
shared_square = shared_var ** 2
function_1 = theano.function([], shared_square)
# print(function_1())

# updates
subtract = T.matrix('subtract')
function_2 = theano.function([subtract], shared_var, updates={shared_var: shared_var - subtract})
# print(shared_var.get_value())
function_2(np.array([[1, 1], [1, 1]], dtype=theano.config.floatX))
# print(shared_var.get_value())
# print(function_1())

# Gradients
bar = foo**2
bar_grad = T.grad(bar, foo)
# print(bar_grad.eval({foo: 10}))
# Recall that y = Ax + b
y_J = theano.gradient.jacobian(y, x)
linear_mix_J = theano.function([A, x, b], y_J)
linear_mix_J_example = linear_mix_J(
    np.array([[9, 8, 7], [4, 5, 6]], dtype=theano.config.floatX),
    np.array([1, 2, 3], dtype=theano.config.floatX),
    np.array([4, 5], dtype=theano.config.floatX)
)
# print(linear_mix_J_example)

num = T.scalar('num')
den = T.scalar('den')
divide = theano.function([num, den], num / den, mode='DebugMode')
# print(divide(0, 0))
