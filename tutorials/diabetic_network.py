# Suppose we have some information about obesity, smoking habits, and exercise habits of five people. We also know whether these people are diabetic or not.
import numpy as np

X = np.array([[0, 1, 0], [0, 0, 1], [1, 0, 0], [1, 1, 0], [1, 1, 1]])
Y = np.array([[1, 0, 0, 1, 1]])
np.random.seed(42)


class Network:
    def __init__(self, X, Y):
        self.X = X
        self.Y = Y

        self.weights = np.random.normal(
            loc=0, scale=1./np.sqrt(len(self.X)), size=(3, 1))
        self.bias = np.random.normal(loc=0, scale=1, size=1)

        self.learning_rate = 0.05
        # self.reg_lambda = 0.0001

    def feed_forward(self):
        activations = sigmoid(np.dot(self.X, self.weights) + self.bias)
        return activations

    def backpropagate(self, activations):
        delta_L = activations - self.Y.T
        # delta_L = (activations - self.Y.T) * sigmoid_prime(activations)

        print 'Error: ' + str(delta_L.sum())

        #  delta_l
        # sum_weights = 0
        # for w in self.weights:
        #     sum_weights += w
        # delta_l = sum_weights * (delta_L * sigmoid_prime(activations))

        derivative_weights = np.dot(self.X.T, delta_L)
        # self.weights = (1-self.learning_rate*self.reg_lambda) * self.weights - \
        # (self.learning_rate * derivative_weights)

        self.weights -= self.learning_rate * derivative_weights

        for num in delta_L:
            self.bias -= self.learning_rate * num

    def train(self):
        activations = self.feed_forward()
        self.backpropagate(activations)

    def test(self):
        single_point = np.array([1, 0, 0])
        result = sigmoid(np.dot(single_point, self.weights) + self.bias)
        print(result)


def sigmoid(x):
    return 1./(1+np.exp(-x))


def sigmoid_prime(x):
    return x*(1-x)


net = Network(X, Y)

for i in range(10000):
    net.train()

net.test()
