import numpy as np
from sklearn import datasets
import matplotlib.pyplot as plt


def collect_data():
    N = 100
    np.random.seed(0)
    X, Y = datasets.make_moons(N, noise=0.10)
    plt.figure(figsize=(7, 4), facecolor='#F3ECF5')
    plt.scatter(X[:, 0], X[:, 1], c=Y, cmap=plt.cm.tab20b)
    Y = Y.reshape(N, 1)
    # plt.show()
    return X, Y, N


def get_activation(a, w, b):
    return sigmoid(np.dot(a, w) + b)


def get_weights(num_input_neurons, num_output_neurons):
    optimized_initialization = 1./np.sqrt(num_input_neurons)
    return np.random.normal(loc=0, scale=optimized_initialization, size=(num_input_neurons, num_output_neurons))


def get_biases(num_input_neurons):
    return np.random.normal(loc=0, scale=1, size=num_input_neurons)


class Network:
    def __init__(self, size_input_layer, size_hidden_layer_1, size_hidden_layer_2, size_output_layer, cost):
        self.cost = cost
        self.X, self.Y, self.N = collect_data()

        self.weights_1 = get_weights(size_input_layer, size_hidden_layer_1)
        self.weights_2 = get_weights(size_hidden_layer_1, size_hidden_layer_2)
        self.output_weights = get_weights(size_hidden_layer_2, size_output_layer)

        self.biases_1 = get_biases(size_hidden_layer_1)
        self.biases_2 = get_biases(size_hidden_layer_2)
        self.output_biases = get_biases(size_output_layer)

    def feedforward(self):
        act_1 = get_activation(self.X, self.weights_1, self.biases_1)
        act_2 = get_activation(act_1, self.weights_2, self.biases_2)
        output_act = get_activation(act_2, self.output_weights, self.output_biases)
        return act_1, act_2, output_act

    def backpropagate(self):
        # Feedforward
        self.act_1, self.act_2, self.output_act = self.feedforward()

        if self.cost == 'MSE':
            # Output error
            output_error = (self.output_act - self.Y) * sigmoid_prime(self.output_act)
            # Backpropagate the error
            error_2 = np.dot(output_error, self.output_weights.T) * sigmoid_prime(self.act_2)
            error_1 = np.dot(error_2, self.weights_2.T) * sigmoid_prime(self.act_1)

            self.learning_rate = 0.8
            self.reg_lambda = 0.0001
            print_error = 0.5*np.linalg.norm(self.output_act-self.Y)**2/self.N

        elif self.cost == 'CRE':
            # Output error
            output_error = self.output_act - self.Y

            # Backpropagate the error
            # error_2 = self.act_2 - self.Y
            # error_1 = self.act_1 - self.Y
            error_2 = np.dot(output_error, self.output_weights.T) * sigmoid_prime(self.act_2)
            error_1 = np.dot(error_2, self.weights_2.T) * sigmoid_prime(self.act_1)

            self.learning_rate = 0.09
            self.reg_lambda = 0.001
            print_error = np.sum(np.nan_to_num(-self.Y*np.log(self.output_act)-(1-self.Y)*np.log(1-self.output_act)))/self.N

        # Gradient descent
        der_output_weights = np.dot(output_error.T, self.act_2)
        der_weights_2 = np.dot(error_2.T, self.act_1)
        der_weights_1 = np.dot(error_1.T, self.X)

        # Regularization L2
        reg_weight = 1 - self.reg_lambda * self.learning_rate/self.N

        self.output_weights = reg_weight * self.output_weights - self.learning_rate * der_output_weights.T
        self.weights_2 = reg_weight * self.weights_2 - self.learning_rate * der_weights_2.T
        self.weights_1 = reg_weight * self.weights_1 - self.learning_rate * der_weights_1.T

        for num in output_error:
            self.output_biases -= self.learning_rate * num
        for num in error_2:
            self.biases_2 -= self.learning_rate * num
        for num in error_1:
            self.biases_1 -= self.learning_rate * num

        print(print_error)

    def train(self):
        for epoch in range(100):
            self.backpropagate()


def sigmoid(x):
    return 1./(1+np.exp(-x))


def sigmoid_prime(x):
    return x * (1-x)


size_input_layer, size_hidden_layer_1, size_hidden_layer_2, size_output_layer = 2, 5, 2, 1

# CRE || MSE
net = Network(size_input_layer, size_hidden_layer_1, size_hidden_layer_2, size_output_layer, cost='MSE')
net.train()
