
import numpy as np


def softmax(x):
    expX = np.exp(x)
    return expX / np.sum(expX, axis=1, keepdims=True)


def sigmoid(x):
    print(x.shape)
    return 1.0 / (1.0 + np.exp(-x))


def sigmoid_prime(x):
    return x * (1 - x)


class SigmoidActivation:
    @staticmethod
    def fn(a, w, b):
        return sigmoid(np.dot(a, w) + b)

    @staticmethod
    def prime(x):
        return sigmoid_prime(x)


class SoftMaxActivation:

    @staticmethod
    def fn(a, w, b):
        return softmax(np.dot(a, w) + b)

    @staticmethod
    def prime(x):
        # For now
        return sigmoid_prime(x)
