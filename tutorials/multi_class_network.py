import numpy as np
import data


def softmax(x):
    expX = np.exp(x)
    return expX / np.sum(expX, axis=1, keepdims=True)


def sigmoid(x):
    return 1. / (1 + np.exp(-x))


def sigmoid_prime(x):
    return x * (1 - x)


class SigmoidActivation:
    @staticmethod
    def fn(a, w, b):
        return sigmoid(np.dot(a, w) + b)

    @staticmethod
    def prime(x):
        return sigmoid_prime(x)


class SoftMaxActivation:

    @staticmethod
    def fn(a, w, b):
        return softmax(np.dot(a, w) + b)

    @staticmethod
    def prime(x):
        # For now
        return sigmoid_prime(x)


class MSECost:
    learning_lambda = 0.005
    reg_eta = 0.01

    @staticmethod
    def fn(a, y, w):
        return 0.5 * np.linalg.norm(a - y)**2 + (CrossEntropyCost.reg_eta * np.sum(w))

    @staticmethod
    def error(a, y, act_fns):
        return (a - y) * act_fns.prime(a)


class CrossEntropyCost:
    learning_lambda = 0.005
    reg_eta = 0.01

    @staticmethod
    def fn(a, y, w):
        return np.sum(-y * np.log(a)) + (CrossEntropyCost.reg_eta * np.sum(w))

    @staticmethod
    def error(a, y, act_fns):
        return (a - y)


class Network:
    def __init__(self, num_neurons_in_layers, cost, data_set, act_fns):
        if (len(act_fns) != len(num_neurons_in_layers) - 1):
            print('Error: The activation functions are not the right number. \n')

        self.cost = cost
        self.act_fns = act_fns
        self.X, self.Y, self.N = data_set()
        self.num_neurons_in_layers = num_neurons_in_layers
        self.num_hidden_layers = len(self.num_neurons_in_layers) - 2
        self.weights, self.biases = self.initialize_parameters()

    def initialize_parameters(self):
        nnl = self.num_neurons_in_layers
        weights = [
            np.random.normal(0, 1. / np.sqrt(nnl[i]), (nnl[i], nnl[i + 1]))
            for i in range(len(nnl) - 1)
        ]
        biases = [
            np.random.normal(0, 1, nnl[i + 1])
            for i in range(len(nnl) - 1)
        ]
        return weights, biases

    def feedforward(self):
        act = self.X
        self.activations = [act]
        for i in range(len(self.act_fns)):
            act = self.act_fns[i].fn(act, self.weights[i], self.biases[i])
            self.activations.append(act)

    def gradient_descent(self):
        # Feedforward
        self.feedforward()

        # Output error
        error_out = self.cost.error(self.activations[-1], self.Y, self.act_fns[-1])

        # Backpropagate the error
        all_errors = [error_out]
        for i in range(self.num_hidden_layers, 0, -1):
            error_out = np.dot(error_out, self.weights[i].T) * self.act_fns[i].prime(self.activations[i])
            all_errors.append(error_out)
        all_errors.reverse()

        # Gradient descent
        derivatives = [np.dot(a.T, err) for a, err in zip(self.activations, all_errors)]

        # Update rules

        # Regularization L2
        reg_weight = 1 - (self.cost.reg_eta * self.cost.learning_lambda / self.N)
        self.weights = \
            [reg_weight * w - self.cost.learning_lambda * d
             for w, d in zip(self.weights, derivatives)]

        self.biases = \
            [b - self.cost.learning_lambda * err.sum(axis=0)
             for b, err in zip(self.biases, all_errors)]

        loss = self.cost.fn(self.activations[-1], self.Y, self.weights[-1])
        return loss

    def train(self):
        error_cost = []
        i = 0
        for epoch in range(10000):
            loss = self.gradient_descent()
            if epoch % 200 == 0:
                print('Loss function value: ', loss)
                error_cost.append(loss)
                i += 1
        data.create_graph(range(i), error_cost)


net = Network(
    num_neurons_in_layers=[2, 4, 5, 3],
    act_fns=[SigmoidActivation, SigmoidActivation, SoftMaxActivation],
    cost=MSECost,
    data_set=data.fetch_multi_class_data
)

net.train()
