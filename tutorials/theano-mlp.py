from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import theano

import theano.tensor as T

print(theano.config.device)


class Layer:
    def __init__(self, W_init, b_init, activation):
        n_output, n_input = W_init.shape
        assert b_init.shape == (n_output,)

        self.W = theano.shared(
            value=W_init.astype(theano.config.floatX),
            name='W',
            borrow=True
        )
        self.b = theano.shared(
            value=b_init.reshape(n_output, 1).astype(theano.config.floatX),
            name='b',
            borrow=True,
            broadcastable=(False, True)  # copied
        )
        self.activation = activation
        self.params = [self.W, self.b]

    def output(self, x):

        lin_output = T.dot(self.W, x) + self.b

        return(
            lin_output if self.activation is None
            else self.activation(lin_output)
        )


class MLP:
    def __init__(self, W_init, b_init, activations):
        assert len(W_init) == len(b_init) == len(activations)

        self.layers = []
        for W, b, activation in zip(W_init, b_init, activations):
            self.layers.append(Layer(W, b, activation))

        self.params = []
        for layer in self.layers:
            self.params += layer.params

    def output(self, x):
        for layer in self.layers:
            x = layer.output(x)
        return x

    def squared_error(self, x, y):
        return T.sum((self.output(x) - y) ** 2)


def gradient_updates_momentum(cost, params, learning_rate, momentum):
    assert momentum < 1 and momentum >= 0
    updates = []
    for param in params:
        previous_step = theano.shared(
            param.get_value() * 0.,
            broadcastable=param.broadcastable)
        step = momentum * previous_step - learning_rate * T.grad(cost, param)
        updates.append((previous_step, step))
        updates.append((param, param + step))
    return updates


np.random.seed(0)
N = 1000
y = np.random.randint(0, 1, N)
means = np.array([[-1, 1], [-1, 1]])
covariance = np.random.random_sample((2, 2)) + 1
X = np.vstack([
    np.random.randn(N) * covariance[0, y] + means[0, y],
    np.random.randn(N) * covariance[1, y] + means[1, y]
]).astype(theano.config.floatX)
y = y.astype(theano.config.floatX)

plt.figure(figsize=(8, 8))
plt.scatter(X[0, :], X[1, :], c=y, lw=.3, s=3, cmap=plt.cm.cool)
plt.axis([-6, 6, -6, 6])
# plt.show()

layer_sizes = [X.shape[0], X.shape[0] * 2, 1]
W_init = []
b_init = []
activations = []
for n_input, n_output in zip(layer_sizes[:-1], layer_sizes[1:]):
    W_init.append(np.random.randn(n_output, n_input))
    b_init.append(np.ones(n_output))
    activations.append(T.nnet.sigmoid)

mlp = MLP(W_init, b_init, activations)

mlp_input = T.matrix('mlp_input')
mlp_target = T.vector('mlp_target')

cost = mlp.squared_error(mlp_input, mlp_target)

learning_rate = 0.0001
momentum = 0.1

train = theano.function(
    [mlp_input, mlp_target],
    mlp.squared_error(mlp_input, mlp_target),
    updates=gradient_updates_momentum(mlp.squared_error(mlp_input, mlp_target), mlp.params, learning_rate, momentum)
)
mlp_output = theano.function([mlp_input], mlp.output(mlp_input))

iteration = 0
max_iteration = 20

while iteration < max_iteration:
    current_cost = train(X, y)
    current_output = mlp_output(X)
    accuracy = np.mean((current_output > .5) == y)

    plt.figure(figsize=(5, 5))
    plt.scatter(X[0, :], X[1, :], c=np.ravel(current_output), lw=.3, s=3, cmap=plt.cm.cool, vmin=0, vmax=1)
    plt.axis([-6, 6, -6, 6])
    plt.title('Cost: {:.3f}, Accuracy: {:.3f}'.format(float(current_cost), accuracy))

    print('Cost: {:.3f}'.format(float(current_cost)))
    print('Accuracy: {:.3f}'.format(accuracy))

    # plt.show()

    iteration += 1
