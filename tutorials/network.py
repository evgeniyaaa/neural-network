
import numpy as np


class Network:
    def __init__(self, x, y):
        self.input = x
        self.weights1 = np.random.rand(self.input.shape[1], 4)
        self.biases1 = np.random.rand(4, 1)
        self.weights2 = np.random.rand(4, 1)
        self.biases2 = np.random.rand(1, 1)
        self.y = y
        self.output = np.zeros(self.y.shape)

    def feedforward(self):
        # if biases are 0
        self.layer1 = sigmoid(
            np.dot(self.input, self.weights1) + self.biases1.T)
        self.output = sigmoid(
            np.dot(self.layer1, self.weights2) + self.biases2.T)

    def backprop(self):
        d_weights2 = np.dot(
            self.layer1.T, (2*(self.y - self.output) * sigmoid_derivative(self.output)))
        d_weights1 = np.dot(self.input.T,  (np.dot(2*(self.y - self.output) * sigmoid_derivative(
            self.output), self.weights2.T) * sigmoid_derivative(self.layer1)))
        self.weights1 += d_weights1
        self.weights2 += d_weights2

        d_biases2 = np.dot(
            np.ones((1, 4)), (2*(self.y - self.output) * sigmoid_derivative(self.output)))
        d_biases1 = np.dot(np.ones((1, 4)),  (np.dot(2*(self.y - self.output) * sigmoid_derivative(
            self.output), self.weights2.T) * sigmoid_derivative(self.layer1)))
        self.biases1 += d_biases1.T
        self.biases2 += d_biases2


def sigmoid(z):
    return 1.0/(1.0 + np.exp(-z))


def sigmoid_derivative(z):
    return z * (1 - z)


if __name__ == "__main__":
    X = np.array([[0, 0, 1],
                  [0, 1, 1],
                  [1, 0, 1],
                  [1, 1, 1]])
    y = np.array([[0], [1], [1], [0]])
    net = Network(X, y)

    for i in range(10500):
        net.feedforward()
        net.backprop()

    print('Result')
    print(net.output)
