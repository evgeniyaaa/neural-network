from matplotlib import pyplot as plt
import numpy as np

feature_set = np.array([[0, 1, 0], [0, 0, 1], [1, 0, 0], [1, 1, 0], [1, 1, 1]])
labels = np.array([[1, 0, 0, 1, 1]])
labels = labels.reshape(5, 1)

np.random.seed(42)
weights = np.random.rand(3, 1)
bias = np.random.rand(1)

lr = 0.05

input = np.linspace(-10, 10, 100)


def sigmoid(x):
    return 1/(1+np.exp(-x))


plt.plot(input, sigmoid(input), c="r")


def sigmoid_der(x):
    return sigmoid(x)*(1-sigmoid(x))


for i in range(10000):
    inputs = feature_set

    # feedforward step1
    XW = np.dot(feature_set, weights) + bias
    # feedforward step2
    z = sigmoid(XW)

    print z

    # backpropagation step 1
    delta_L = z - labels
    print delta_L.sum()
    # print 'delta_L'
    # print delta_L

    # backpropagation step 2
    delta_l = delta_L * sigmoid_der(z)

    inputs = feature_set.T
    weights -= lr * np.dot(inputs, delta_l)

    for num in delta_l:
        bias -= lr * num
