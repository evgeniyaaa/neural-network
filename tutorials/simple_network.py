import numpy as np


def sigmoid(x, deriv=False):
    if(deriv == True):
        return x*(1-x)

    return 1/(1+np.exp(-x))


X = np.array([[0, 0, 1],
              [0, 1, 1],
              [1, 0, 1],
              [1, 1, 1]])

y = np.array([[0],
              [1],
              [1],
              [0]])

np.random.seed(1)

# randomly initialize our weights with mean 0
weights0 = 2*np.random.random((3, 4)) - 1
weights1 = 2*np.random.random((4, 1)) - 1

for j in xrange(60000):

    # Feed forward through layers 0, 1, and 2
    a0 = X
    a1 = sigmoid(np.dot(a0, weights0))
    a2 = sigmoid(np.dot(a1, weights1))

    # how much did we miss the target value?
    a2_error = y - a2

    if (j % 10000) == 0:
        print "Error:" + str(np.mean(np.abs(a2_error)))

    # in what direction is the target value?
    # were we really sure? if so, don't change too much.
    a2_delta = a2_error*sigmoid(a2, deriv=True)

    # how much did each a1 value contribute to the a2 error (according to the weights)?
    a1_error = a2_delta.dot(weights1.T)

    # in what direction is the target a1?
    # were we really sure? if so, don't change too much.
    a1_delta = a1_error * sigmoid(a1, deriv=True)

    weights1 += a1.T.dot(a2_delta)
    weights0 += a0.T.dot(a1_delta)
